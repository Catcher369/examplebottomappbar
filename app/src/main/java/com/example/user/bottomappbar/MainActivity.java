package com.example.user.bottomappbar;

import android.support.design.bottomappbar.BottomAppBar;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        setSupportActionBar((BottomAppBar) findViewById(R.id.bottom_app_bar));
        FloatingActionButton lFloatingActionButton = findViewById(R.id.fab);
        lFloatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher_foreground, null));
        lFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View pView) {
                new BottomNavigationDrawerFragment()
                        .show(getSupportFragmentManager(), BottomNavigationDrawerFragment.class.getName());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.menu.bottom_nav_drawer_menu:
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.bottom_nav_drawer_menu, menu);
        return true;
    }
}
